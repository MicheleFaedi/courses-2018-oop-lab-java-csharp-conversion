package it.unibo.oop.mnk;

import java.util.Random;

public enum Symbols {
    EMPTY("_"),
    CROSS("X"),
    ROUND("O");

    private static final Random RAND = new Random();
    private final String representation;

    Symbols(String repr) {
        representation = repr;
    }

    public static Symbols pickRandomly(Symbols... symbols) {
        if (symbols.length == 0) {
            return null;
        } else {
            return symbols[RAND.nextInt(symbols.length)];
        }
    }

    public static Symbols pickRandomly() {
        return pickRandomly(Symbols.ROUND, Symbols.CROSS);
    }

    @Override
    public String toString() {
        return representation;
    }
}
