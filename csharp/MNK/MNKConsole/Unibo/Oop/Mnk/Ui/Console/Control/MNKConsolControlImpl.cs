using System;
using System.IO;

namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    internal class MNKConsolControlImpl : IMNKConsoleControl
    {
        public IMNKMatch Model { get; set; }

        public void Input()
        {
            while (true)
            {
                string line = System.Console.ReadLine();
                String[] coords = line.Split(new string[] { "\\s+" }, StringSplitOptions.None); // split using one or more spaces as delimiters
                if (line.ToLower() == "reset")
                {
                    Model.Reset();
                }
                else if (line == "exit")
                    System.Environment.Exit(1);
                else if (coords.Length == 2)
                {
                    String fst = coords[0];
                    String snd = coords[1];
                    int i = fst[0] - 'a';
                    int j;
                    if (!int.TryParse(snd, out j))
                        System.Console.WriteLine("Input error");
                    else
                    {
                        j -= 1;
                        MakeMove(i, j);
                    }
                }
            }
        }
        public void MakeMove(int i, int j)
        {
            if (Model != null)
                Model.Move(i, j);
        }
    }
}