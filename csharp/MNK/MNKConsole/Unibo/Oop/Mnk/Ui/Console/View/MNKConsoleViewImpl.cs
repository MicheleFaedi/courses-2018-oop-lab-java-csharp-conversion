﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk.Ui.Console.View
{
    class MNKConsoleViewImpl : IMNKConsoleView
    {
        public IMNKMatch Model { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void RenderEnd(IMNKMatch model, int turn, Symbols? winner, IImmutableMatrix<Symbols> state)
        {
            throw new NotImplementedException();
        }

        public void RenderNextTurn(IMNKMatch model, int turn, Symbols player, IImmutableMatrix<Symbols> grid)
        {
            System.Console.WriteLine("MNK-Game %dx%dx%d -- Turn %d -- Player: %s\n", grid.GetRowsSize(), grid.getColumnsSize(), m.getK(), turn, player.name());

            System.Console.WriteLine("\t\t  ");
            System.Console.WriteLine(
                    IntStream.rangeClosed(1, grid.getColumnsSize())
                            .mapToObj(String::valueOf)
                            .collect(Collectors.joining(" "))
            );

            for (int i = 0; i < grid.getRowsSize(); i++)
            {
                System.Console.WriteLine("\t\t");
                System.Console.WriteLine(int2Char(i));
                System.Console.WriteLine(" ");
                System.Console.WriteLine(renderRow(grid, i));
            }

            System.Console.WriteLine("\tWhere do you want to put a %s? (pattern: <row> <column>)\n\t> ", player);
        }
    }
}
