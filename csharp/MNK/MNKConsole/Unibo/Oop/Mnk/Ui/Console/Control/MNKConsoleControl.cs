using System;

namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    public static class MNKConsoleControl
    {
        public static IMNKConsoleControl Of(IMNKMatch match)
        {
            return new MNKConsolControlImpl() { Model = match };
        }
        
        public static IMNKConsoleControl Of()
        {
            return new MNKConsolControlImpl();
        }
    }
}