﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSourceImpl<T> : IEventSource<T>, IEventEmitter<T>
    {
        public IEventSource<T> EventSource=> this;

        protected abstract List<EventListener<T>> getEventListeners();
        public void Bind(EventListener<T> eventListener)
        {
            getEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<T> eventListener)
        {
            getEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            getEventListeners().Clear();
        }

        public void Emit(T data)
        {
            foreach (EventListener<T> e in getEventListeners())
            {
                e(data);
            }
        }
    }
}
