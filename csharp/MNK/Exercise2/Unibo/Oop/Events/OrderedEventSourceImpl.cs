﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedEventSourceImpl<T> : AbstractEventSourceImpl<T>
    {
        private List<EventListener<T>> eventListeners = new List<EventListener<T>>();

        protected override List<EventListener<T>> getEventListeners()
        {
            return eventListeners;
        }
    }
}
