using System;
using System.Collections.Generic;

namespace Unibo.Oop.Utils
{
    public interface IBaseMatrix<TElem> : IEnumerable<TElem>
    {
        int ColumnsCount { get; }
        int RowsCount { get; }
        int Count { get; }

        int CoordDiagonal(int i, int j);
        int CoordAntidiagonal(int i, int j);
        int CoordRow(int d, int a);
        int CoordColumn(int d, int a);
        
        TElem this[int i, int j] { get; }
        TElem GetDiagonals(int d, int a);

        IEnumerable<TElem> GetRow(int i);
        IEnumerable<TElem> GetColumn(int j);
        
        IEnumerable<TElem> GetDiagonal(int d);
        IEnumerable<TElem> GetAntidiagonal(int a);

        void ForEachIndexed(Action<int, int, TElem> consumer);

        TElem[] ToArray();
        IList<TElem> ToList();
    }
}