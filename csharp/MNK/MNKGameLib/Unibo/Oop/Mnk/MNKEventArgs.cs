using System;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public abstract class MNKEventArgs
    {
        public IMNKMatch Source { get; private set; }
        public int Turn { get; private set; }
        public Symbols Player { get; private set; }
        public Tuple<int, int> Move { get; private set; }
        public MNKEventArgs(IMNKMatch source, int turn, Symbols player, int i, int j)
        {
            Source = source;
            Turn = turn;
            Player = player;
            Move = new Tuple<int, int>(i, j);
        }

    }
}