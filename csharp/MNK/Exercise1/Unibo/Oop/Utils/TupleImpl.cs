﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private object[] elements;
        public TupleImpl(object[] args)
        {
            elements = args;
        }

        public object this[int i] { get { return elements[i]; } }

        public int Length { get { return elements.Length; } }

        public object[] ToArray()
        {
            return elements;
        }
        public override bool Equals(object obj)
        {
            return obj != null
                && obj is TupleImpl
                && Enumerable.SequenceEqual(elements, ((TupleImpl)obj).elements);
        }
        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            foreach (Object obj in elements)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }
        public override string ToString()
        {
            return "(" + String.Join(", ", elements) + ")";
        }
    }
}
